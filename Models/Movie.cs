using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace MvcMovie.Models {
    public class Movie {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Título")]
        public string Title { get; set; }
        
        [Required]
        [Display(Name = "Duración")]
        public string Runtime { get; set; }

        [Required]
        [Display(Name = "Fecha de estreno")]
        [DataType(DataType.Date)]
        public string Released { get; set; }
        
        [Required]
        [Display(Name = "Resumen")]
        public string Plot { get; set; }
        
        [Required]
        [Display(Name = "Poster")]
        public string Poster { get; set; }
        
        [Required]
        [Display(Name = "Genero")]
        public string Genre { get; set; }
        
        [NotMapped]
        public List<string> Images { get; set; }

        [Display(Name = "Imágenes en Json")]
        [Column(TypeName = "json")]
        public string ImagesJson { get; set; }
        public List<Comments> Comments { get; set; }

        public Movie() {
            Comments = new List<Comments>();
        }
    }
}