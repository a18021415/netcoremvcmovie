﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function loadComment(id, event) {
    $('.modal').modal('show');

    event.preventDefault();

    fetch('https://localhost:5001/Comments/Index/' + id)
    .then(response => response.text())
    .then(html => {
        document.getElementById("div_comment").innerHTML = html;
    });
}

$(document).ready(function() {
    $(".toast").toast('show');
});

function deleteMovie(id) {
    Swal.fire({
        title: 'Error!',
        text: 'Do you want to continue',
        icon: 'error',
        showDenyButton: true,
        confirmButtonText: 'Eliminar',  
        denyButtonText: 'Cancelar',
    }).then((result) => {  
        /* Read more about isConfirmed, isDenied below */  
        if (result.isConfirmed) {    
            window.location.href = "https://localhost:5001/Movie/Delete/" + id;
        } 
    });
}