using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MvcMovie.Models;

namespace MvcMovie.ViewComponents
{
    public class CommentsViewComponent : ViewComponent
    {
        public List<Comments> lstComments = null;
        public CommentsViewComponent()
        {
            var myJsonString = System.IO.File.ReadAllText("Models/Comments.json");
            lstComments = JsonConvert.DeserializeObject<List<Comments>>(myJsonString);
        }

        public async Task<IViewComponentResult> InvokeAsync(int num = 0)
        {
            return View(lstComments.Take(num).ToList());
        }
    }
}